<!doctype html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Styles -->
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            
            <div class="container">

                <table class="table table-bordered">
                    <thead>
                        <th>IP</th>
                        <th>Importe</th>
                        <th>Iva</th>
                    </thead>
                <tbody>
                
                    <tr>
                        <td>172.20.1.240</td>
                        <td>{{number_format($uno->sum('doctotal')-$uno->sum('vatsum'),2,".",",")}}</td>
                        <td>{{number_format($uno->sum('vatsum'),2,".",",")}}</td>
                    </tr>
                    
                    <tr>
                        <td>192.168.1.246</td>
                        <td>{{number_format($dos->sum('doctotal')-$uno->sum('vatsum'),2,".",",")}}</td>
                        <td>{{number_format($dos->sum('vatsum'),2,".",",")}}</td>
                    </tr>
                    <tr>
                        <td>192.168.1.248</td>
                        <td>{{number_format($tres->sum('doctotal')-$uno->sum('vatsum'),2,".",",")}}</td>
                        <td>{{number_format($tres->sum('vatsum'),2,".",",")}}</td>
                    </tr>
                    <tr>
                        <td>192.168.1.249</td>
                        <td>{{number_format($cuatro->sum('doctotal')-$uno->sum('vatsum'),2,".",",")}}</td>
                        <td>{{number_format($cuatro->sum('vatsum'),2,".",",")}}</td>
                    </tr>
                    <tr>
                        <td>192.168.1.247</td>
                        <td>{{number_format($cinco->sum('doctotal')-$uno->sum('vatsum'),2,".",",")}}</td>
                        <td>{{number_format($cinco->sum('vatsum'),2,".",",")}}</td>
                    </tr>
                -->
                 
                </tbody>
                    <tfoot>
                        <th>Total</th>
                        <th>{{ number_format(
                            $uno->sum('doctotal')-$uno->sum('vatsum')+
                            <!--$dos->sum('doctotal')-$dos->sum('vatsum') +-->
                            <!--$tres->sum('doctotal')-$tres->sum('vatsum') + -->
                            <!--$cuatro->sum('doctotal')-$cuatro->sum('vatsum') + -->
                            <!--$cinco->sum('doctotal')-$cinco->sum('vatsum'),2,".",",") -->
                        }}</th>
                        <th>{{ number_format(   
                            $uno->sum('vatsum'),2,".",",")
                            <!--$dos->sum('vatsum') + -->
                            <!--$tres->sum('vatsum') +-->
                            <!--$cuatro->sum('vatsum') + -->
                            <!--$cinco->sum('vatsum'),2,".",",") -->
                        }}</th>
                    </tfoot>
                </table>
            
                <a href="{{url('/server')}}" class="btn btn-primary">Por Servidor</a>
                <a href="{{url('/clientes')}}" class="btn btn-info">Por Cliente</a>
                <a href="{{url('/proveedores')}}" class="btn btn-warning">Por Proveedor</a>

                <br>
                <br>
                <br>
                <br>

            </div>
        </div>

    </body>
</html>
