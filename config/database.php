<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'uno' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.21',
            'port' => '1433',
            'database' => 'EPPOS8',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'dos' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.22',
            'port' => '1433',
            'database' => 'EPPOS7',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'tres' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.23',
            'port' => '1433',
            'database' => 'EPPOS6',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'cuatro' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.24',
            'port' => '1433',
            'database' => 'EPOS5',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'cinco' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.25',
            'port' => '1433',
            'database' => 'EPPOS4',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'seis' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.26',
            'port' => '1433',
            'database' => 'EPPOS2',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'siete' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.27',
            'port' => '1433',
            'database' => 'EPPOS3',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'ocho' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.7.28',
            'port' => '1433',
            'database' => 'EPPOS1',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        /*
        'nueve' => [
            'driver' => 'sqlsrv',
            'host' => '172.20.1.249',
            'port' => '1433',
            'database' => 'EP9POS',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'diez' => [
            'driver' => 'sqlsrv',
            'host' => '172.20.1.250',
            'port' => '1433',
            'database' => 'EP10POS',
            'username' => 'sa',
            'password' => 'Evi12345',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        */

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
