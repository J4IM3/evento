<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EventoController extends Controller
{

    protected $servers;

    public function __construct()
    {
       // $this->servers = ['uno'];
    $this->servers = ['uno','dos','tres','cuatro','cinco','seis','siete','ocho'];
    }

    public function clientes()
    {
        $clientes = $this->getClientes();
        return view('clientes', compact('clientes'));
    }


    public function getClientes(){
        DB::table('clientes')->truncate();
        $sql = "select cardcode, cardname, sum(doctotal)-sum(vatsum) as importe, sum(vatsum) as iva
                from osbs
                where docdate >= '20190426'
                group by cardcode, cardname";

        foreach ($this->servers as $server){
            $data = DB::connection($server)->select($sql);
           $this->setClientes($data);
        }

        $sql = 'SELECT codigo, nombre, SUM(importe) as importe, SUM(iva) as iva
                FROM clientes
                GROUP BY codigo, nombre
                ORDER BY SUM(importe) DESC';

        return collect(DB::select($sql));
    }

    public function setClientes($data)
    {
        foreach ($data as $d){
            $this->inserta('clientes', $d);
        }
    }

    public function inserta($table, $d)
    {
        DB::table($table)->insert([
            'codigo' => $d->cardcode,
            'nombre' => $d->cardname,
            'importe'=> $d->importe,
            'iva'    => $d->iva
        ]);
    }

    /**
     * Proveedores
     */

    public function proveedores()
    {
        $proveedores = $this->getProveedores();
        return view('proveedores', compact('proveedores'));
    }

    private function getProveedores()
    {
        DB::table('proveedores')->truncate();
        $sql = "select p.cardcode, p.cardname, sum(d.linetotal) as importe, sum(d.vatsum) as iva
                from osbs c
                inner join sbs1 d on c.docentry = d.docentry
                inner join oitm a on a.itemcode = d.itemcode
                inner join ocrd p on p.cardcode = a.cardcode
                where c.docdate >= '20181006'
                group by p.cardcode, p.cardname";

        foreach ($this->servers as $server){
            $data = DB::connection($server)->select($sql);
            $this->setProveedores($data);
        }

        $sql = 'SELECT codigo, nombre, SUM(importe) as importe, SUM(iva) as iva
                FROM proveedores
                GROUP BY codigo, nombre
                ORDER BY SUM(importe) DESC';

        return collect(DB::select($sql));
    }

    public function setProveedores($data)
    {
        foreach ($data as $d){
            $this->inserta('proveedores', $d);
        }
    }

    public function reporte()
    {
        $data = $this->getReporte();
        return view('reporte');
    }

    private function getReporte()
    {
        DB::table('reporte')->truncate();
        $sql = "select Q.cardcode,Q.cardname,a.cardcode as codigo_proveedor,p.cardname as nombre_proveedor,d.itemcode,a.itemname,d.quantity ,d.linetotal,d.vatsum,d.unitmsr,q.number,
            (select posdescription from TERMINAL where IdTerminal=Q.terminalid) as maquina
            from osbs q
            inner join sbs1 D on d.docentry=Q.docentry
            inner join oitm A on A.itemcode=D.itemcode
            left join OCRD P on P.cardcode=A.cardcode
            where Q.docdate between '20181006' and '20181006'";

        foreach ($this->servers as $server) {
            $data = DB::connection($server)->select($sql);
            $this->setReporte($data);
            
        }
    }

    public function setReporte($data)
    {
        foreach($data as $d)
        {
             DB::table('reporte')->insert([
                'cliente' => $d->cardcode,
                'nombre' => $d->cardname,
                'codigo_proveedor' => $d->codigo_proveedor,
                'nombre_proveedor' => $d->nombre_proveedor,
                'codigo_articulo' => $d->itemcode,
                'nombre_articulo' => $d->itemname,
                'importe' => $d->linetotal,
                'iva'    => $d->vatsum,
                'cantidad' => $d->quantity,
                'unidad' => $d->unitmsr,
                'cotizacion' => $d->number,
                'maquina' => $d->maquina
            ]);
        }
    }
    /*
    *cliente, nombre, proveedor , nombre, articulo, nombre, linetotal, vatsum,quantity, unitmsr,number(osbs),
    *
    */

}
