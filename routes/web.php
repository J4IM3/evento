<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return view('resultados');
});


Route::get('/server', function(){
    $uno = DB::connection('uno')->table('osbs')
        ->select('doctotal','vatsum')
        ->where('docdate','>=','20190426')
        ->get();

    $dos = DB::connection('dos')->table('osbs')
        ->select('doctotal','vatsum')->get();

    $tres = DB::connection('tres')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $cuatro = DB::connection('cuatro')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $cinco = DB::connection('cinco')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $seis = DB::connection('seis')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $siete = DB::connection('siete')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $ocho = DB::connection('ocho')->table('osbs')
        ->select('doctotal','vatsum')->get();/*
    $nueve = DB::connection('nueve')->table('osbs')
        ->select('doctotal','vatsum')->get();
    $diez = DB::connection('diez')->table('osbs')
        ->select('doctotal','vatsum')->get();*/
    //return view('servidor', compact('uno'));
    return view('servidor', compact('uno','dos','tres','cuatro','cinco','seis','siete','ocho'));
});

Route::get('/clientes',[
    'uses' => 'EventoController@clientes'
]);

Route::get('/proveedores',[
    'uses' => 'EventoController@proveedores'
]);

Route::get('/reporte',[
    'uses'=> 'EventoController@reporte'
]);